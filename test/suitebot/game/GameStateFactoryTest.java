package suitebot.game;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class GameStateFactoryTest
{
	@Rule
	public final ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testCreatingFromValidString() throws Exception
	{
		String gameStateAsString =
				"*2 *\n" +
				"4 +*\n" +
				"!   \n";

		GameState gameState = GameStateFactory.createFromString(gameStateAsString);

		assertThat(gameState.getPlanWidth(), is(4));
		assertThat(gameState.getPlanHeight(), is(3));
		assertThat(gameState.getAllBotIds(), containsInAnyOrder(2, 4));
		assertThat(gameState.getLiveBotIds(), containsInAnyOrder(2, 4));
		assertThat(gameState.getBotLocation(2), equalTo(new Point(1, 0)));
		assertThat(gameState.getBotLocation(4), equalTo(new Point(0, 1)));
		assertThat(gameState.getBotEnergy(2), is(GameStateFactory.DEFAULT_BOT_ENERGY));
		assertThat(gameState.getBotEnergy(4), is(GameStateFactory.DEFAULT_BOT_ENERGY));
		assertThat(gameState.getObstacleLocations(), containsInAnyOrder(
				new Point(0, 0), new Point(3, 0), new Point(3, 1)));
		assertThat(gameState.getBatteryLocations(), containsInAnyOrder(new Point(2, 1)));
		assertThat(gameState.getTreasureLocations(), containsInAnyOrder(new Point(0, 2)));
	}

	@Test
	public void invalidCharacter_shouldThrowException() throws Exception
	{
		expectedException.expect(GameStateFactory.GameStateCreationException.class);
		expectedException.expectMessage("unrecognized character: #");

		GameStateFactory.createFromString("**1#");
	}

	@Test
	public void nonRectangularPlan_shouldThrowException() throws Exception
	{
		expectedException.expect(GameStateFactory.GameStateCreationException.class);
		expectedException.expectMessage("non-rectangular plan: line 3 width (3) is different from the line 1 width (4)");

		GameStateFactory.createFromString("****\n****\n***\n****");
	}
}
